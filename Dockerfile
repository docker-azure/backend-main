FROM php:7.4-fpm-alpine

WORKDIR /var/www

RUN apk update && apk add --no-cache \
  freetype \
  libpng \
  libjpeg-turbo \
  freetype-dev \
  libpng-dev \
  vim \
  libzip-dev \
  unzip \
  git \
  curl \
  zip \
  jpegoptim optipng pngquant gifsicle \
  libzip-dev \
  libjpeg-turbo-dev

RUN docker-php-ext-configure gd \
  --with-freetype --with-jpeg
RUN docker-php-ext-install pdo pdo_mysql exif pcntl gd bcmath zip

# Install PHP Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Copy upload.ini to config
COPY uploads.ini /usr/local/etc/php/conf.d/uploads.ini

COPY ./code/database/ database/

# Copy composer.lock and composer.json
COPY ./code/composer.lock ./code/composer.json /var/www/

RUN composer install \
  --no-interaction \
  --no-plugins \
  --no-scripts \
  --no-dev \
  --prefer-dist

# Copy existing application directory contents
COPY ./code/ /var/www

RUN composer install
# RUN composer dump-autoload

RUN chown -R www-data:www-data .

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
