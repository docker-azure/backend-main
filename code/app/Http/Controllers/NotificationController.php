<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\NotificationService;
use App\Models\User;
use Exception;

class NotificationController extends Controller
{
    protected $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function save_token(Request $req)
    {
        $result = $this->notificationService->sendNotification([$req->token]);
        try {
            if (auth()->user()) {
                $this->notificationService->saveUser($req);
                return $this->successResponse($result);
            } else {
                $this->notificationService->saveGuest($req);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    public function add(Request $req)
    {
        $users = User::has("notifications")->get();
        return view("admin.noti.add")->with(compact(['users']));
    }

    public function send(Request $req)
    {
        if ($req->user_id == "SEND_ALL") {
            $notis = $this->notificationService->getAll();
        } else {
            $notis = $this->notificationService->getTokenByUser($req);
        }

        $tokens = [];
        foreach ($notis as $noti) {
            $tokens[] = $noti->token;
        }

        try {
            $this->notificationService->sendNotification($tokens, ["title" => $req->title, "body" => $req->body, "image" => $req->image]);
            return redirect()->route('noti.add')->with('msg', 'Gửi thông báo thành công');
        } catch (Exception $e) {
            // return redirect()->route('noti.add')->with('msg', $e->getMessage());
            return redirect()->route('noti.add')->with('msg', "Gửi thông báo thất bại");
        }
    }
}
