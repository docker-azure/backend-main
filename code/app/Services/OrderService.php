<?php

namespace App\Services;

use App\Models\Statistic;
use App\Repositories\OrderRepository;
use App\Repositories\OrderDetailRepository;
use App\Repositories\CartRepository;
use App\Repositories\ShippingRepository;
use App\Repositories\FeeshipRepository;
use App\Repositories\CouponRepository;
use App\Repositories\UnpaidRepository;
use App\Repositories\StatisticRepository;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use PDF;
use Carbon\Carbon;
use Dotenv\Result\Success;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Exception;
use Excel;
use Illuminate\Support\Str;

class OrderService
{
    protected $orderRepository;
    protected $cartRepository;
    protected $shippingRepository;
    protected $orderDetailRepository;
    protected $couponRepository;
    protected $unpaidRepository;
    protected $feeshipRepository;
    protected $statisticRepository;

    public function __construct(StatisticRepository $statisticRepository, FeeshipRepository $feeshipRepository, UnpaidRepository $unpaidRepository, CouponRepository $couponRepository, OrderDetailRepository $orderDetailRepository, OrderRepository $orderRepository, CartRepository $cartRepository, ShippingRepository $shippingRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
        $this->shippingRepository = $shippingRepository;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->couponRepository = $couponRepository;
        $this->unpaidRepository = $unpaidRepository;
        $this->feeshipRepository = $feeshipRepository;
        $this->statisticRepository = $statisticRepository;
    }

    public function getCount()
    {
        return $this->orderRepository->getCount();
    }

    public function getAll()
    {
    }

    public function getAllByAdmin()
    {
        return $this->orderRepository->getAllByAdmin();
    }

    public function getById($id)
    {
        return $this->orderRepository->getById($id);
    }

    public function getByStatus($status)
    {
        if ($status === "all") {
            return $this->orderRepository->getAll();
        }
        return $this->orderRepository->getByStatus($status);
    }

    public function getByCode($code)
    {
        return $this->orderRepository->getByCode($code);
    }

    protected function insertOrder($data)
    {
        $this->orderRepository->save($data);
    }

    protected function insertShipping($data)
    {
        $this->shippingRepository->save((object)$data);
    }

    protected function insertOrderDetail($order_code)
    {
        $this->orderDetailRepository->save($order_code);
    }

    protected function useCoupon($coupon_code)
    {
        if ($coupon_code != "NO") {
            $this->couponRepository->useCoupon($coupon_code);
        }
    }

    public function updateStatusByCode($data)
    {
        return $this->orderRepository->updateStatusByCode($data->code, $data->status);
    }

    protected function handleNewOrder($data)
    {
        $this->insertOrder($data);
        $this->insertShipping($data);
        $this->insertOrderDetail($data['code']);
        $this->useCoupon($data['coupon_code']);
    }

    public function save($data)
    {
        $data_order = [
            'code' =>  strtoupper(substr(md5(microtime()), rand(0, 26), 8)),
            'name' => $data->name,
            'phone' => $data->phone,
            'note' => $data->note,
            'method' => $data->method,
            'coupon_code' =>  $data->coupon_code,
            'feeship_money' => $data->feeship,
            'address' => $data->address,
        ];

        $product_money = $this->cartRepository->getTotalPriceChecked();
        $total_money = $product_money +  $data_order["feeship_money"];
        $coupon_money = 0;


        if ($data->coupon_code !== "NO") {
            $coupon = $this->couponRepository->getByCode($data->coupon_code);
            if ($coupon) {
                $coupon_money = $coupon->money;
            }
        }

        $status = $data->method == "0" ? "processing" : "awaiting_payment";
        $total_money = $total_money - $coupon_money;

        $data_order["status"] = $status;
        $data_order["coupon_money"] = $coupon_money;
        $data_order["product_money"] = $product_money;
        $data_order["total_money"] = $total_money;

        $url_paypal = "";

        if ($data->method === 3) {
            $provider = new PayPalClient;
            $provider->setApiCredentials(config('paypal'));
            $paypalToken = $provider->getAccessToken();

            $usd = 0.0000430156;

            $items = [];
            $carts = auth()->user()->carts()->where('checked', "CHECK")->get();

            foreach ($carts as $cart) {
                $items[] = [
                    "name" => $cart->product->name,
                    "quantity" =>  $cart->quantity,
                    "description" => Str::limit($cart->product->description, 124),
                    "value" => number_format($cart->product->price * $usd, 0, '.', ''),
                ];
            }

            $product_money = 0;
            $feeship_money = number_format($data_order["feeship_money"] * $usd, 0, '.', '');

            foreach ($items as $item) {
                $product_money = $product_money + ($item["value"] * $item["quantity"]);
            }

            $money_usd = $product_money +  $feeship_money;

            $order_title = 'Thanh toán cho đơn hàng trị giá ' .   $money_usd . ' USD tại hệ thống MW Store - ' . Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');


            $response = $provider->createOrder([
                "intent" => "CAPTURE",
                "application_context" => [
                    'brand_name' => config('app.name'),
                    "return_url" => env('RETURN_URL_PAYMENT_CLIENT'),
                    "cancel_url" => env('RETURN_URL_PAYMENT_CLIENT'),
                    "shipping_preference" => 'SET_PROVIDED_ADDRESS',
                ],
                "purchase_units" => [
                    0 => [
                        "amount" => [
                            "currency_code" => "USD",
                            "value" => $money_usd,
                            "breakdown" => [
                                'item_total' => [
                                    'currency_code' => 'USD',
                                    'value' => $product_money,
                                ],
                                'shipping' => [
                                    'currency_code' => 'USD',
                                    'value' => $feeship_money,
                                ],
                                'discount' => [
                                    'currency_code' => 'USD',
                                    'value' => '0',
                                ]
                            ]
                        ],
                        "description" => $order_title,
                        "items" => array_map(function ($item) {
                            return [
                                "name" => $item['name'],
                                "quantity" => $item['quantity'],
                                "description" => $item['description'],
                                "unit_amount" => [
                                    "currency_code" => 'USD',
                                    "value" => $item['value'],
                                ]
                            ];
                        }, $items),
                        "shipping" => [
                            'name' =>
                            [
                                'full_name' => auth()->user()->name,
                            ],
                            'address' =>
                            [
                                'country_code' => 'VN',
                                'address_line_1' => 'Việt Nam',
                                'admin_area_1' => 'Đường...',
                                'admin_area_2' =>  $data_order["address"],
                            ],
                        ]
                    ]
                ],

            ]);

            $url_paypal = [$response, $items, $product_money, $feeship_money,  $money_usd];

            if (isset($response['id']) && $response['id'] != null) {
                $data_order["code"] = $response['id'];

                // redirect to approve href
                foreach ($response['links'] as $links) {
                    if ($links['rel'] == 'approve') {
                        $url_paypal = $links['href'];
                    }
                }
            }
        }

        $this->handleNewOrder($data_order);
        // $this->cartRepository->deleteChecked();

        $order = $this->orderRepository->getByCode($data_order["code"]);

        return ["order" => $order, "url_paypal" => $url_paypal];
    }

    public function paymentMomo($data)
    {
        $url_resp = [];

        $order_title = 'Thanh toán mua hàng tại hệ thống MW Store - ' . Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');

        $response = \MoMoAIO::purchase([
            'amount' => $data->total_money,
            'returnUrl' =>  env('RETURN_URL_PAYMENT_CLIENT'),
            'notifyUrl' => env('RETURN_URL_PAYMENT_CLIENT'),
            'orderId' => $data->code,
            'requestId' =>  $data->code,
            'orderInfo' => $order_title,
        ])->send();

        if ($response->isRedirect()) {
            $redirectUrl = $response->getRedirectUrl();

            $url_resp = ["type" => "MOMO", "url" => $redirectUrl];
        }

        return $url_resp;
    }

    public function paymentVNPay($data)
    {
        $url_resp = [];
        $order_title = 'Thanh toán mua hàng tại hệ thống MW Store - ' . Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');


        $vnpay = \VNPay::purchase([
            "vnp_TxnRef" =>  $data->code,
            "vnp_OrderInfo" => $order_title,
            "vnp_Amount" =>  $data->total_money * 100,
            'vnp_OrderType' => 100000,
            "vnp_IpAddr" =>  $_SERVER['REMOTE_ADDR'],
            "vnp_ReturnUrl" => env('RETURN_URL_PAYMENT_CLIENT'),
        ])->send();


        if ($vnpay->isRedirect()) {
            $redirectUrl = $vnpay->getRedirectUrl();
            $url_resp = ["type" => "VNPAY", "url" => $redirectUrl];
        }

        return $url_resp;
    }

    public function callbackPayment($data)
    {
        if (!$data->has('vnp_ResponseCode') && !$data->has('errorCode') && !$data->has('token') && !$data->has('PayerID')) {
            throw new Exception("111");
        }

        if ($data->has('vnp_ResponseCode') && $data->vnp_ResponseCode != '00') {
            throw new Exception("222");
        }

        if ($data->has('errorCode') && $data->errorCode != 0) {
            throw new Exception("333");
        }

        $code = isset($data->vnp_TxnRef) ? $data->vnp_TxnRef : "";
        $code = isset($data->orderId)  ? $data->orderId : $code;

        if ($data->has('token') && $data->has('PayerID')) {
            $provider = new PayPalClient;
            $provider->setApiCredentials(config('paypal'));
            $provider->getAccessToken();
            $response = $provider->capturePaymentOrder($data['token']);

            if (!isset($response['status']) || $response['status'] != 'COMPLETED') {
                throw new Exception("444");
            }
            $code = $response['id'];
        }

        if ($code == "") {
            throw new Exception('Not Found Code');
        }

        DB::beginTransaction();

        try {
            $order = $this->orderRepository->updateStatusByCode($code, "processing");
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new Exception('FAIL');
        }

        DB::commit();

        return $order;
    }

    public function confirmOrder($data)
    {
        DB::beginTransaction();

        try {
            $order = $this->orderRepository->getByCode($data->code);
            $order->status = 1;
            $order->save();

            $order_date = Carbon::parse($order->time)->format('Y-m-d');

            $sales = $order->total_order;
            $profit = $sales * 0.3;
            $total_order = 1;
            $quantity = 0;

            $order_details = $order->orderDetails;
            foreach ($order_details as $order_detail) {
                $quantity += $order_detail->product_quantity;
            }

            $this->statisticRepository->updateOrSave($order_date, $sales, $profit, $quantity, $total_order);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new Exception($e->getMessage());
        }

        DB::commit();
    }

    public function delete($data)
    {
        DB::beginTransaction();

        try {
            $brand = $this->orderRepository->delete($data->code);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());

            throw new Exception('FAIL');
        }

        DB::commit();

        return $brand;
    }

    public function exportPDF($code)
    {
        $str = base64_decode($code);

        try {
            $print = explode("--", $str);
            $order = $this->orderRepository->getByPrint($print);

            if ($order) {
                $pdf = PDF::loadView('admin.order.print', compact('order'))->setPaper('a4', 'landscape');
                $file = $pdf->download('MW_Store_' . $order->code . '.pdf')->getOriginalContent();
                return $file;
            }

            throw new Exception('FAIL');
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
