importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"
);

const firebaseConfig = {
    apiKey: "AIzaSyDU2edSdegoFs11a_DocHIp8oY9h0pFPv0",
    authDomain: "mwstore-notification.firebaseapp.com",
    databaseURL: "https://mwstore-notification-default-rtdb.firebaseio.com",
    projectId: "mwstore-notification",
    storageBucket: "mwstore-notification.appspot.com",
    messagingSenderId: "508025420900",
    appId: "1:508025420900:web:ae088a50c2a0b029307371",
    measurementId: "G-MMTX4705BM",
};

firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

self.addEventListener("notificationclick", function (event) {
    console.log("Event ", event);
    event.notification.close();
    event.waitUntil(self.clients.openWindow(event.notification.data.url));
});

messaging.onBackgroundMessage(function (payload) {
    console.log("Handling background message ", payload);

    return self.registration.showNotification(payload.notification.title, {
        body: payload.notification.body,
        icon: "./firebase-logo.png",
        tag: payload.notification.tag,
        data: payload.data,
        image: payload.notification.image,
        actions: payload.notification.actions,
    });
});
