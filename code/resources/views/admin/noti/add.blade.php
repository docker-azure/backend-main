@extends('admin.layouts.master')
@section('title', 'Thêm thông báo')
@section('title_page', 'Thêm thông báo')
@section('sub_title_page', 'Thêm thông báo')

@section('content_page')
<script>
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>

<div class="card-body">
    <h5 class="card-title">Thêm thông báo</h5>
    <form class="needs-validation" action="{{route('noti.send')}}" method="POST" enctype="multipart/form-data" novalidate>
        @csrf
        <div class="form-row">
            <div class="col-md-12 mb-3">
                <label for="validationTooltip01">Người nhận</label>
                <select class="mb-2 form-control" name="user_id" required>
                    <option value="SEND_ALL">Tất cả</option>
                    @foreach ($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-6 mb-3">
                <label for="validationTooltip01">Tiêu đề thông báo</label>
                <input type="text" class="form-control" id="validationTooltip01" name="title" placeholder="Tiêu đề thông báo" value="" required>
                <div class="invalid-feedback">Vui lòng nhập đầy đủ dữ liệu.</div>
                @if ($errors->has('title'))
                <div class="">{{ $errors->first('title') }}</div>
                @endif
                <div class="valid-feedback">Tuyệt vời!!!</div>
            </div>

            <div class="col-md-6 mb-3">
                <label for="validationTooltip01">Nôi dung thông báo</label>
                <input type="text" class="form-control" id="validationTooltip01" name="body" placeholder="Nội dung thông báo" value="" required>
                <div class="invalid-feedback">Vui lòng nhập đầy đủ dữ liệu.</div>
                @if ($errors->has('description'))
                <div class="">{{ $errors->first('description') }}</div>
                @endif
                <div class="valid-feedback">Tuyệt vời!!!</div>
            </div>

            <div class="col-md-12 mb-3">
                <label for="validationTooltip01">Liên kết hình ảnh (không bắt buộc)</label>
                <input type="text" class="form-control" id="validationTooltip01" name="image" placeholder="Liên kết hình ảnh" value="" required>
                <div class="invalid-feedback">Vui lòng nhập đầy đủ dữ liệu.</div>
                @if ($errors->has('image'))
                <div class="">{{ $errors->first('image') }}</div>
                @endif
                <div class="valid-feedback">Tuyệt vời!!!</div>
            </div>
        </div>

        <div class="form-row text-center">
            <div class="col-md-12 mb-3 mt-2">
                @if (session()->has('msg'))
                <p class="alert alert-success">{{session('msg')}}</p>
                @endif

                <button class="btn btn-success " name="submit" style="padding-left: 35px; padding-right:35px ;" type="submit">Gủi thông báo</button>
            </div>
        </div>

    </form>
</div>
@endsection
